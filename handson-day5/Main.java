import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        int numberOfCodes = 10;
        int codeLength = 10;
        
        Set<String> voucherCodes = generateUniqueVoucherCodes(numberOfCodes, codeLength);

        System.out.println("\nKode Voucher: \n");
        int index = 1;
        for (String code : voucherCodes) {
            System.out.println("Voucher #" + index++ + ": " + code);
        }
    }

    private static Set<String> generateUniqueVoucherCodes(int numberOfCodes, int codeLength) {
        // Memastikan semua unique menggunakkan HashSet
        Set<String> voucherCodes = new HashSet<>();
        Random random = new Random();

        // Selama yang unik kurang dari jumlah, terus generate kode voucher
        while (voucherCodes.size() < numberOfCodes) {
            StringBuilder codeBuilder = new StringBuilder();

            // Membangun karakter kode voucher berdasarkan ukuran (10)
            for (int i = 0; i < codeLength; i++) {
                // Generate random kode ascii dari 0 - 56
                // 0 - 25 untuk A-Z
                // 26 + 55 untuk a-z (dikurang 6 karena dalam kode ascii lowercase lompat sedikit)
                int randomAsciiNumber = random.nextInt(56);
                int adjustedAsciiNumber;
                // jika kode ascii diantara kedua ini, akan menghasilkan karakter yang bukan alphabet
                if(randomAsciiNumber > 25 && randomAsciiNumber < 32) {
                    // skip 6 karena bukan character A-Z
                    adjustedAsciiNumber = randomAsciiNumber + 6;
                }else {
                    adjustedAsciiNumber = randomAsciiNumber;
                }

                // Awal kode ascii untuk A adalah 26. Jadi nomor random + 'A' akan menghasilkan character alphabet
                char randomChar = (char) (adjustedAsciiNumber + 'A');
                codeBuilder.append(randomChar);
            }

            String voucherCode = codeBuilder.toString();
            voucherCodes.add(voucherCode);
        }

        return voucherCodes;
    }
}